# Spline

Fork of the spline libraries by J. Burkardt.

https://people.sc.fsu.edu/~jburkardt/c_src/spline/spline.html

Licensed under the GNU LGPL license.