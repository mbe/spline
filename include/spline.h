// https://people.sc.fsu.edu/~jburkardt/c_src/spline/spline.html
// GPLv2!

#ifndef INCLUDE_WATCHER_SPLINE
#define INCLUDE_WATCHER_SPLINE

#if ( defined WIN32 ) && !( defined SPLINE_STATIC )
   #ifdef SPLINE_EXPORT
      #define SPLINE_API __declspec( dllexport )
   #else
      #define SPLINE_API __declspec( dllimport )
   #endif
#else
   #define SPLINE_API
#endif

float SPLINE_API basis_function_b_val ( float tdata[], float tval );

float SPLINE_API basis_function_beta_val(float beta1, float beta2, float tdata[], float tval );

SPLINE_API float *basis_matrix_b_uni(void);

SPLINE_API float *basis_matrix_beta_uni(float beta1, float beta2);

SPLINE_API float *basis_matrix_bezier(void);

SPLINE_API float *basis_matrix_hermite(void);

SPLINE_API float *basis_matrix_overhauser_nonuni(float alpha, float beta);

SPLINE_API float *basis_matrix_overhauser_nul(float alpha);

SPLINE_API float *basis_matrix_overhauser_nur(float beta);

SPLINE_API float *basis_matrix_overhauser_uni(void);

SPLINE_API float *basis_matrix_overhauser_uni_l(void);

SPLINE_API float *basis_matrix_overhauser_uni_r(void);

SPLINE_API float basis_matrix_tmp(int left, int n, float mbasis[], int ndata,
						  float tdata[], float ydata[], float tval );

SPLINE_API void bc_val(int n, float t, float xcon[], float ycon[], float *xval,
			  float *yval );

SPLINE_API float bez_val(int n, float x, float a, float b, float y[]);

SPLINE_API float bp_approx(int n, float a, float b, float ydata[], float xval);

SPLINE_API float *bp01(int n, float x);

SPLINE_API float *bpab(int n, float a, float b, float x);

SPLINE_API float d_max(float x, float y);

SPLINE_API float d_min(float x, float y);

SPLINE_API float d_random(float rlo, float rhi, int *seed);

SPLINE_API float d_uniform_01(int *seed);

SPLINE_API int d3_fs(float a1[], float a2[], float a3[], int n, float b[], float x[]);

SPLINE_API float *d3_mxv(int n, float a[], float x[]);

SPLINE_API float *d3_np_fs(int n, float a[], float b[]);

SPLINE_API void d3_print(int n, float a[], char *title);

SPLINE_API void d3_print_some(int n, float a[], int ilo, int jlo, int ihi, int jhi);

SPLINE_API float *d3_random(int n, int *seed);

SPLINE_API void data_to_dif(float diftab[], int ntab, float xtab[], float ytab[]);

SPLINE_API void dif_val(float diftab[], int ntab, float xtab[], float xval,
			   float *yval );

SPLINE_API void dvec_bracket(int n, float x[], float xval, int *left, int *right);

SPLINE_API void dvec_bracket3(int n, float t[], float tval, int *left);

SPLINE_API float *dvec_even(int n, float alo, float ahi);

SPLINE_API float *dvec_indicator(int n);

SPLINE_API void dvec_order_type(int n, float x[], int *order);

SPLINE_API void dvec_print(int n, float a[], char *title);

SPLINE_API float *dvec_random(int n, float alo, float ahi, int *seed);

SPLINE_API void dvec_sort_bubble_a(int n, float a[]);

SPLINE_API int i_max(int i1, int i2);

SPLINE_API int i_min(int i1, int i2);

SPLINE_API void least_set(int ntab, float xtab[], float ytab[], int ndeg,
				float ptab[], float b[], float c[], float d[], float *eps, int *ierror );

SPLINE_API float least_val(float x, int ndeg, float b[], float c[], float d[]);

SPLINE_API void parabola_val2(int ndim, int ndata, float tdata[], float ydata[],
					int left, float tval, float yval[] );

SPLINE_API int s_len_trim(char* s);

SPLINE_API float spline_b_val(int ndata, float tdata[], float ydata[], float tval);

SPLINE_API float spline_beta_val(float beta1, float beta2, int ndata, float tdata[],
						 float ydata[], float tval );

SPLINE_API float spline_constant_val(int ndata, float tdata[], float ydata[], float tval);

SPLINE_API float* spline_cubic_set(
	const int n,
	const float t[],
	const float y[],
	const int ibcbeg,
	const float ybcbeg, 
	const int ibcend,
	const float ybcend );

SPLINE_API float spline_cubic_val(
	const int n, 
	const float t[], 
	const float tval, 
	const float y[], 
	const float ypp[],
	float *ypval,
	float *yppval );

SPLINE_API float spline_cubic_val(
	const int n, 
	const float t[], 
	const float tval, 
	const float y[], 
	const float ypp[]);
SPLINE_API void spline_cubic_val2(int n, float t[], float tval, int *left, float y[],
						float ypp[], float *yval, float *ypval, float *yppval );

SPLINE_API float *spline_hermite_set(int ndata, float tdata[], float ydata[],
							float ypdata[] );

SPLINE_API void spline_hermite_val(int ndata, float tdata[], float c[], float tval,
						  float *sval, float *spval );

SPLINE_API float spline_linear_int(int ndata, float tdata[], float ydata[], float a, float b);

SPLINE_API void spline_linear_intset(int int_n, float int_x[], float int_v[],
							float data_x[], float data_y[] );

SPLINE_API void spline_linear_val(int ndata, float tdata[], float ydata[],
						float tval, float *yval, float *ypval );

SPLINE_API float spline_overhauser_nonuni_val(int ndata, float tdata[],
									  float ydata[], float tval );

SPLINE_API float spline_overhauser_uni_val(int ndata, float tdata[], float ydata[],
								  float tval );

SPLINE_API void spline_overhauser_val(int ndim, int ndata, float tdata[], float ydata[],
							 float tval, float yval[] );

SPLINE_API void spline_quadratic_val(int ndata, float tdata[], float ydata[],
							float tval, float *yval, float *ypval );

SPLINE_API void timestamp(void);

#endif // INCLUDE_WATCHER_SPLINE